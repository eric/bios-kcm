#!/bin/sh

BUILD_DIR="~/kde/build/bios-kcm"
cmake -B $BUILD_DIR -DCMAKE_INSTALL_PREFIX=~/kde/usr
cmake --build $BUILD_DIR
cmake --install $BUILD_DIR

source ~/kde/build/kcm-bios/prefix.sh

kcmshell6 kcm_bios
