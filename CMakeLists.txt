# SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
# SPDX-FileCopyrightText: 2024 Akseli Lahtinen <akselmo@akselmo.dev>
#
# SPDX-License-Identifier: BSD-2-Clause

cmake_minimum_required(VERSION 3.16)
project(bios-kcm)

set(QT_MIN_VERSION "6.5.0")
set(KF6_MIN_VERSION "6.0.0")
set(PROJECT_DEP_VERSION "6.0.80")
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

add_definitions(-DTRANSLATION_DOMAIN="kcm_bios")

remove_definitions(-DQT_NO_CAST_FROM_ASCII -DQT_NO_CAST_FROM_BYTEARRAY -DQT_NO_KEYWORDS)
add_definitions(-DQT_NO_NARROWING_CONVERSIONS_IN_CONNECT)
add_definitions(-DQT_NO_URL_CAST_FROM_STRING)
add_definitions(-DQT_USE_QSTRINGBUILDER)

find_package(ECM ${KF6_MIN_VERSION} REQUIRED NO_MODULE)

set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake )

include(KDEInstallDirs)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(KDECMakeSettings)
include(KDEGitCommitHooks)
include(KDEClangFormat)
include(FeatureSummary)
include(ECMDeprecationSettings)

ecm_set_disabled_deprecation_versions(QT 6.4.0
    KF 5.240
)

find_package(Qt6 ${QT_MIN_VERSION} CONFIG REQUIRED Core Quick Gui Qml)

find_package(KF6 ${KF6_MIN_VERSION} REQUIRED COMPONENTS
    Config
    Auth
    CoreAddons
    KCMUtils
    I18n
)

#find_package(LibKWorkspace CONFIG REQUIRED)
#find_package(LibKWorkspace ${PROJECT_DEP_VERSION} REQUIRED)

kauth_install_actions(org.kde.kcm.plasma.bios kcm_bios.actions)

add_executable(kcmbios_authhelper 
    biosauthhelper.cpp 
    biosauthhelper.h
)

target_link_libraries(kcmbios_authhelper 
    KF6::AuthCore 
    KF6::I18n
)

kauth_install_helper_files(kcmbios_authhelper org.kde.kcm.plasma.bios root)
install(TARGETS kcmbios_authhelper DESTINATION ${KAUTH_HELPER_INSTALL_DIR})

add_subdirectory(src)

ki18n_install(po)

file(GLOB_RECURSE ALL_CLANG_FORMAT_SOURCE_FILES *.cpp *.h)
kde_clang_format(${ALL_CLANG_FORMAT_SOURCE_FILES})

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
kde_configure_git_pre_commit_hook(CHECKS CLANG_FORMAT)