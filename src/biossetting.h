// SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <QString>
#include <QVariant>
#include <QList>
#include <QObject>

class BiosSetting : public QObject {

    Q_OBJECT

    Q_PROPERTY(QString name READ name CONSTANT)

    Q_PROPERTY(QString description READ description CONSTANT)

    Q_PROPERTY(bool readOnly READ readOnly CONSTANT)

    Q_PROPERTY(QString currentValue READ currentValue WRITE setCurrentValue NOTIFY currentValueChanged)

    Q_PROPERTY(QList<QString> values READ values CONSTANT)

    public:
        BiosSetting(QString name, QString description, bool readOnly, QString currentValue, QList<QString> values, QObject *parent = nullptr);

        QString name() const;

        QString description() const;

        bool readOnly() const;

        QList<QString> values() const;

        QString currentValue() const;

        void setCurrentValue(QString currentValue);

    Q_SIGNALS:
        void currentValueChanged(QString newValue);

    private:
        QString m_name;
        QString m_description;
        bool m_readOnly;

        QString m_currentValue;
        QList<QString> m_values;
};