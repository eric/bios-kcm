// SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include "biossettingsmodel.h"

#include <KQuickConfigModule>

using namespace Qt::Literals::StringLiterals;

class BiosModule : public KQuickConfigModule
{
    Q_OBJECT

    Q_PROPERTY(QPointer<BiosSettingsModel> settingsModel READ settingsModel CONSTANT)

    public:
        BiosModule(QObject *parent, const KPluginMetaData &data);

        QPointer<BiosSettingsModel> settingsModel();

    public Q_SLOTS:
        void load() override;
        void save() override;

    private:
        QPointer<BiosSettingsModel> m_settingsModel;
        int m_index = -1;
};
