// SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "kcm_bios.h"
#include "biossettingsmodel.h"

#include <KPluginFactory>

using namespace Qt::Literals::StringLiterals;

K_PLUGIN_CLASS_WITH_JSON(BiosModule, "kcm_bios.json")

BiosModule::BiosModule(QObject *parent, const KPluginMetaData &data)
    : KQuickConfigModule(parent, data)
{
    m_settingsModel = new BiosSettingsModel{};
    setButtons(Help | Apply);

    constexpr const char *uri{"org.kde.plasma.bios.kcm"};

    qmlRegisterType<BiosSettingsModel>(uri, 1, 0,"BiosSettingsModel");
    qmlRegisterType<BiosSetting>(uri, 1, 0, "BiosSetting");

    // ask for do you want to reboot the system now after applying the settings?
    // let the user also reboot into firmware from bios kcm?
}

QPointer<BiosSettingsModel> BiosModule::settingsModel() {
    return m_settingsModel;
}

void BiosModule::load() {
    qDebug() << m_settingsModel;
    m_settingsModel->load();
    setNeedsSave(false);
}

void BiosModule::save() {
    m_settingsModel->save();
}




#include "kcm_bios.moc"
