// SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "biossetting.h"

BiosSetting::BiosSetting(QString name, QString description, bool readOnly,
                         QString currentValue, QList<QString> values,
                         QObject *parent)
    : QObject(parent), m_name(name), m_description(description),
      m_readOnly(readOnly), m_currentValue(currentValue), m_values(values) {}


QString BiosSetting::name() const {
    return m_name;
}

QString BiosSetting::description() const {
    return m_description;
}

bool BiosSetting::readOnly() const {
    return m_readOnly; 
}

QString BiosSetting::currentValue() const {
    return m_currentValue;
}

QList<QString> BiosSetting::values() const {
    return m_values;
}

void BiosSetting::setCurrentValue(QString currentValue) {
    if (m_currentValue != currentValue) {
        m_currentValue = currentValue;
        Q_EMIT currentValueChanged(currentValue);
    }
}
