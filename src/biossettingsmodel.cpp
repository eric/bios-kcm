// SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "biossettingsmodel.h"
#include "kcm_bios_debug.h"

#include <KAuth/ExecuteJob>
#include <KAuth/Action>

#include <memory>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <memory>

using namespace Qt::Literals::StringLiterals;

BiosSettingsModel::BiosSettingsModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_data{}
{
}

int BiosSettingsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_data.size();
}

QVariant BiosSettingsModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    const auto &biosSetting = m_data.at(index.row());

    switch(role) {
        case Qt::DisplayRole:
            return biosSetting->name();
        case Roles::CurrentValueRole:
            return biosSetting->currentValue();
        case Roles::ValuesRole:
            return biosSetting->values();
        case Roles::DescriptionRole:
            return biosSetting->description();
    }

    return {};
}

QHash<int, QByteArray> BiosSettingsModel::roleNames() const {
    QHash<int, QByteArray> map = {
        {Roles::CurrentValueRole, "currentValue"},
        {Roles::ValuesRole, "values"},
        {Roles::DescriptionRole, "description"}
    };

    return map;
}

void BiosSettingsModel::reset(std::vector<std::unique_ptr<BiosSetting>> &biosSettings) {
    beginResetModel();
    m_data.swap(biosSettings);
    endResetModel();
}


QString BiosSettingsModel::errorText() const {
    return m_errorText;
}

void BiosSettingsModel::setErrorText(const QString &errorText) {
    if (errorText != m_errorText) {
         m_errorText = errorText;
         Q_EMIT errorTextChanged(m_errorText);  
    }
}

void BiosSettingsModel::load() {
    KAuth::Action readAction(u"org.kde.kcm.plasma.bios.read"_s);
    readAction.setHelperId(u"org.kde.kcm.plasma.bios"_s);

    KAuth::ExecuteJob *job = readAction.execute();

    if (!job->exec()) {
        qDebug() << "KAuth returned an error code:" << job->error();
        setErrorText(job->errorText());
    } else {
        qDebug() << job->data();
        QString contents = job->data()[u"contents"_s].toString();
        qDebug() << "KAuth read bios settings succeeded. Contents: " << contents;

        QString warning = job->data()[u"warning"_s].toString();
        setErrorText(warning);

        auto biosSettings = fromJson(contents);
        reset(biosSettings);
    }
}

void BiosSettingsModel::save() {
    KAuth::Action writeAction(u"org.kde.kcm.plasma.bios.write"_s);
    writeAction.setHelperId(u"org.kde.kcm.plasma.bios"_s);

    const auto args = QVariantMap {{QStringLiteral("payload"), toJson(m_data)}};
    writeAction.setArguments(args);

    KAuth::ExecuteJob *job = writeAction.execute();

    if (!job->exec()) {
        //qCDebug(KCMBIOS) << "KAuth returned an error code:" << job->error();
        setErrorText(job->errorText());
    } else {
        //qCDebug(KCMBIOS) << "KAuth write bios settings succeeded";
    }
}

std::vector<std::unique_ptr<BiosSetting>> BiosSettingsModel::fromJson(const QString &json) {

    std::vector<std::unique_ptr<BiosSetting>> ret;
    const auto jsonDoc = QJsonDocument::fromJson(json.toUtf8());
    if (!jsonDoc.isObject()) {
        return ret;
    }

    // See implementation details here: https://github.com/fwupd/fwupd/blob/main/docs/bios-settings.md
    const auto biosSettingsObj = jsonDoc.object();
    if (biosSettingsObj[u"BiosSettings"_s].isArray()) {
        const auto biosSettingsArray = biosSettingsObj[u"BiosSettings"_s].toArray();
        
        for (const auto &biosSettingItem : biosSettingsArray) {
            const auto biosSetting = biosSettingItem.toObject();
            const auto name = biosSetting[u"Name"_s].toString();
            const auto description = biosSetting[u"Description"_s].toString();
            const auto filename = biosSetting[u"Filename"_s].toString();
            //const auto id = biosSetting[u"BiosSettingId"_s].toString();
            //const auto type = biosSetting[u"BiosSettingType"_s].toInt();
            const auto currentValue = biosSetting[u"BiosSettingCurrentValue"_s].toString();
            const auto readOnly = biosSetting[u"BiosSettingReadOnly"_s].toBool();
            const auto possibleValues = biosSetting[u"BiosSettingPossibleValues"_s].toArray();
            
            QList<QString> values;
            for (const auto &possibleValue : possibleValues) {
                values.emplace_back(possibleValue.toString());
            }
            
            ret.push_back(std::make_unique<BiosSetting>(name, description, readOnly, currentValue, values));
        }
    }
    return ret;
}

QString BiosSettingsModel::toJson(const std::vector<std::unique_ptr<BiosSetting>> &settings) {
    QJsonArray settingsArray;
    for (const auto &biosSetting : settings) {
        QJsonObject settingObj;
        settingObj[u"Name"_s] = biosSetting->name();
        settingObj[u"Description"_s] = biosSetting->description();
        settingObj[u"BiosSettingCurrentValue"_s] = biosSetting->currentValue();
        settingObj[u"BiosSettingReadOnly"_s] = biosSetting->readOnly();

        QJsonArray possibleValuesArray;
        for (const auto &value : biosSetting->values()) {
            possibleValuesArray.push_back(value);
        }

        settingObj[u"BiosSettingPossibleValues"_s] = possibleValuesArray;
        settingsArray.push_back(settingObj);
    }

    auto retDoc = QJsonDocument{QJsonObject { { u"BiosSettings"_s, settingsArray }}}.toJson(QJsonDocument::Compact);
    
    return QString::fromUtf8(retDoc);
}

