# SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
#
# SPDX-License-Identifier: BSD-2-Clause

kcmutils_add_qml_kcm(kcm_bios)

include(ECMQtDeclareLoggingCategory)

ecm_qt_declare_logging_category(
  DEBUG_SOURCES
  HEADER kcm_bios_debug.h
  IDENTIFIER KCMBIOS
  CATEGORY_NAME "kcm_bios"
)

target_sources(kcm_bios PRIVATE
   biossettingsmodel.cpp
   biossettingsmodel.h
   biossetting.cpp
   biossetting.h
   kcm_bios.cpp
   kcm_bios.h
)

target_link_libraries(kcm_bios PRIVATE
    Qt::Quick
    KF6::CoreAddons
    KF6::KCMUtils
    KF6::I18n
    KF6::AuthCore
)

