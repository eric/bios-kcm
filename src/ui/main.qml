// SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
//
// SPDX-License-Identifier: LGPL-3.0-only

import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts

import org.kde.kirigami as Kirigami
import org.kde.kcmutils as KCMUtils
import org.kde.kitemmodels as KItemModels

import org.kde.plasma.bios.kcm

KCMUtils.ScrollViewKCM {
    id: root
    title: i18n("BIOS Settings")

    implicitWidth: Kirigami.Units.gridUnit * 40
    implicitHeight: Kirigami.Units.gridUnit * 20
    framedView: false

    Kirigami.Theme.colorSet: Kirigami.Theme.View
    Kirigami.Theme.inherit: false


    header: Kirigami.SearchField {
        id: filterField
        // KeyNavigation.tab: appsListView
        // KeyNavigation.down: appsListView
        autoAccept: false
        onAccepted: {
            if (text === "") {
                // The signal also fires when user clicks the "Clear" action/icon/button.
                // In this case don't treat it as a command to open first app.
                return;
            }
            if (filteredRefsModel.count >= 0) {
                appsListView.setCurrentIndexLater(0);
                root.shouldChange(0);
            }
        }
    }

    BiosSettingsModel {
        id: biosSettingsModel
    }

    KItemModels.KSortFilterProxyModel {
        id: filterModel
        sourceModel: biosSettingsModel
        sortOrder: Qt.AscendingOrder
        sortCaseSensitivity: Qt.CaseInsensitive
        sortRoleName: "Name"
        filterRoleName: "Name"
        filterString: filterField.text
        filterCaseSensitivity: Qt.CaseInsensitive
        // onRowCountChanged: {
        //     if (count >= 0) {
        //         const sourceRowIndex = root.KCM.ConfigModule.currentIndex();
        //         appsListView.setCurrentIndexLater(sourceRowIndex);
        //     }
        // }
    }

    view: Kirigami.CardsListView {
        id: settingsList
        visible: biosSettingsModel.rowCount() !== 0
        model: filterModel
        delegate: Kirigami.AbstractCard {
            id: biosSettingsDelegate
            
            header: Kirigami.Heading {
                text: filterModel.name
                level: 2
            }

            contentItem: Item {
                implicitWidth: delegateLayout.implicitWidth
                implicitHeight: delegateLayout.implicitHeight
                
                GridLayout {
                    id: delegateLayout
                    anchors {
                        left: parent.left
                        top: parent.top
                        right: parent.right
                    }
                    
                    rowSpacing: Kirigami.Units.largeSpacing
                    columnSpacing: Kirigami.Units.largeSpacing
                    columns: width > Kirigami.Units.gridUnit * 20 ? 4 : 2

                    QQC2.Label {
                        Layout.fillWidth: true
                        wrapMode: Text.WordWrap
                        text: filterModel.description
                        visible: filterModel.description.length > 0
                    }
                    
                    // QQC2.ComboBox {

                    //     Layout.alignment: Qt.AlignRight
                    //     Layout.columnSpan: 2 
                    //     text: 
                    // }
                }
            }
        }
    }

    Kirigami.PlaceholderMessage {
        anchors.centerIn: parent

        visible: biosSettingsModel.rowCount() === 0
        width: parent.width - (Kirigami.Units.largeSpacing * 4)

        text: biosSettingsModel.errorText.length != 0 ? biosSettingsModel.errorText : i18nc("@info:text", "Reading the bios settings failed for an unknown reason.")
    }
}
