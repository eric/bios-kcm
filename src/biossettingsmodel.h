// SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include "biossetting.h"

#include <vector>

#include <QAbstractItemModel>
#include <QString>


class BiosSettingsModel : public QAbstractListModel

{
    Q_OBJECT

    Q_PROPERTY(QString errorText READ errorText WRITE setErrorText NOTIFY errorTextChanged)

public:
    
    BiosSettingsModel(QObject *parent = nullptr);

    enum Roles {

        CurrentValueRole = Qt::UserRole + 1,

        ValuesRole,

        DescriptionRole,

    };

    Q_ENUM(Roles)

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role) const override;

    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void reset(std::vector<std::unique_ptr<BiosSetting>> &biosSettings);

    void load();

    void save();

    QString errorText() const;

    void setErrorText(const QString &errorText);

Q_SIGNALS:
    void errorTextChanged(const QString &newErrorText);

private:

    std::vector<std::unique_ptr<BiosSetting>> fromJson(const QString &json);

    QString toJson(const std::vector<std::unique_ptr<BiosSetting>> &json);

    QString m_errorText;

    std::vector<std::unique_ptr<BiosSetting>> m_data;
};