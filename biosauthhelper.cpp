// SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
// SPDX-FileCopyrightText: 2021-2022 Harald Sitter <sitter@kde.org>
//
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#include "biosauthhelper.h"

#include <QDebug>
#include <QIODevice>
#include <QJsonDocument>
#include <QProcess>
#include <QStandardPaths>
#include <QTemporaryFile>

#include <KLocalizedString>

// See implementation details here: https://github.com/fwupd/fwupd/blob/main/docs/bios-settings.md

BiosAuthHelper::BiosAuthHelper() {
    m_fwupdmgrPath = QStandardPaths::findExecutable(QStringLiteral("fwupdmgr"));
}

ActionReply BiosAuthHelper::read(const QVariantMap &args) {
    Q_UNUSED(args);
    ActionReply reply;

    auto proc = new QProcess(this);
    proc->setProcessChannelMode(QProcess::MergedChannels);
    
    connect(proc, &QProcess::finished, this, [proc, &reply](int /* exitCode */, QProcess::ExitStatus exitStatus) {
        proc->deleteLater();

        switch (exitStatus) {
            case QProcess::CrashExit:
                reply = ActionReply::HelperErrorReply();
                reply.setErrorDescription(i18nc("@info", "Fwupdmgr crashed while reading BIOS settings."));
                return;
            case QProcess::NormalExit:
                break;
        }

        auto contents = QString::fromLocal8Bit(proc->readAllStandardOutput());
        // there may be warnings first, we can ignore those
        auto data = contents.right(contents.length() - contents.indexOf(QStringLiteral("{")));
        // better be safe, maybe there are also warnings after the json payload to ignore
        data = data.left(data.lastIndexOf(QStringLiteral("}")) + 1);

        if (contents.startsWith(QStringLiteral("WARNING"))) {
            const auto warning = contents.left(contents.indexOf(QStringLiteral("{")));
            reply.addData(QStringLiteral("warning"), warning);
        }

        reply.addData(QStringLiteral("contents"), contents);
    });

    proc->start(m_fwupdmgrPath, {QStringLiteral("get-bios-setting"), QStringLiteral("--json")});
    proc->waitForFinished();

    return reply;
}

ActionReply BiosAuthHelper::write(const QVariantMap &args) {
    ActionReply reply;
    QString tempFileName;

    {
        QTemporaryFile temp;
        if(!temp.open()) {
            reply = ActionReply::HelperErrorReply();
            reply.setErrorDescription(i18nc("@info", "Failed writing BIOS settings update to temporary file"));
            return reply;
        }

        QString json_payload = args[QStringLiteral("payload")].toString();
        QTextStream stream(&temp);
        stream << json_payload << '\n';
        tempFileName = temp.fileName();
    }

    auto proc = new QProcess(this);
    proc->setProcessChannelMode(QProcess::MergedChannels);
    connect(proc, &QProcess::finished, this, [proc, &reply](int /* exitCode */, QProcess::ExitStatus exitStatus) {
        proc->deleteLater();

        switch (exitStatus) {
            case QProcess::CrashExit:
                reply = ActionReply::HelperErrorReply();
                reply.setErrorDescription(i18nc("@info", "Fwupdmgr crashed while updating BIOS settings."));
                return;
            case QProcess::NormalExit:
                break;
        }
    });

    QStringList procArgs = {QStringLiteral("set-bios-setting"), QStringLiteral("--json"), tempFileName};
    proc->start(m_fwupdmgrPath, procArgs);

    return reply;
}

KAUTH_HELPER_MAIN("org.kde.kcm.plasma.bios", BiosAuthHelper)
