// SPDX-FileCopyrightText: 2024 Eric Armbruster <eric1@armbruster-online.de>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <KAuth/ActionReply>
#include <KAuth/HelperSupport>
#include <QObject>

using namespace KAuth;

class BiosAuthHelper : public QObject
{
    Q_OBJECT
public:
    BiosAuthHelper();

public Q_SLOTS:
    ActionReply read(const QVariantMap &args);
    ActionReply write(const QVariantMap &args);

private:

    QString m_fwupdmgrPath;
};
